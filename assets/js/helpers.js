function buildDOMFromElement( eTitle, eDescription, eLink){
    //create each element
    var container = document.createElement("div");
    container.classList.add('result-container');
    container.setAttribute("style", "background-color:green");
    
    var checkbox = document.createElement("input");
    checkbox.classList.add('select');
    checkbox.type = "checkbox";
    
    var title = document.createElement("P")
    title.classList.add('title');
    title.innerHTML = eTitle;
    
    var description = document.createElement("P")
    description.classList.add('description');
    description.innerHTML = eDescription;
    
    var link = document.createElement("a");
    link.classList.add('url');
    link.href = eLink;
    link.innerHTML = eLink;

    container.appendChild(checkbox);
    container.appendChild(title);
    container.appendChild(link);
    container.appendChild(description);

    checkbox.addEventListener('click', function(){
        container.classList.toggle('selected');
    });

    return container;
}

function downloadFile( type ){
    var selectedElements = document.querySelectorAll('.result-container.selected');
    var results = [];
    
    selectedElements.forEach(element => {
        var title = element.querySelector('.title').innerText;
        var description = element.querySelector('.description').innerText;
        var url = element.querySelector('.url').innerText;
        results.push( {
            title: title,
            description: description,
            url : url,
        } );
    });

    switch (type) {
        case 'json':
            download( JSON.stringify(results), 'result.' + type, 'text/plain');
            break;
        case 'xml':
            download( JsonToXml(results), 'result.' + type, 'text/plain');
            break;
        case 'csv':
                download( JsonToCsv(results), 'result.' + type, 'text/plain');
            break;
    }
}

function download(text, name, type) {
    console.log('here');
    var a = document.getElementById("download");
    a.classList.add('active');
    var file = new Blob([text], {type: type});
    a.href = URL.createObjectURL(file);
    a.download = name;
}

function JsonToXml(obj) {
    var xml = '';
    for (var prop in obj) {
      xml += obj[prop] instanceof Array ? '' : "<" + prop + ">";
      if (obj[prop] instanceof Array) {
        for (var array in obj[prop]) {
          xml += "<" + prop + ">";
          xml += JsonToXml(new Object(obj[prop][array]));
          xml += "</" + prop + ">";
        }
      } else if (typeof obj[prop] == "object") {
        xml += JsonToXml(new Object(obj[prop]));
      } else {
        xml += obj[prop];
      }
      xml += obj[prop] instanceof Array ? '' : "</" + prop + ">";
    }
    var xml = xml.replace(/<\/?[0-9]{1,}>/g, '');
    return xml
}

function JsonToCsv(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (var index in array[i]) {
            if (line != '') line += ','

            line += array[i][index];
        }

        str += line + '\r\n';
    }

    return str;
}

function selectAll(){
    var containers = document.querySelectorAll('.result-container');
    containers.forEach(container => {
        if(!container.classList.contains('selected')){
            container.classList.add('selected');
            container.querySelector('.select').checked = true;
        }
    });
}

function deselectAll(){
    var containers = document.querySelectorAll('.result-container');
    containers.forEach(container => {
        if(container.classList.contains('selected')){
            container.classList.remove('selected');
            container.querySelector('.select').checked = false;
        }
    });
}