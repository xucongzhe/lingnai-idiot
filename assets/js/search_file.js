// Search from File
function uploadFile(event){
    var fileName = document.getElementById('myInput');
    var choseFile =event.target.files[0];
    var readFile = new FileReader();
    var display =document.getElementsByClassName('results')[0];
     
    readFile.onload =function(e){
        var fileText = readFile.result;
        var extend = fileName.value.split(".")[1];
            
        mydisplay(fileText,extend,display);
    };
    
    readFile.readAsText(choseFile);
}

function mydisplay(fileText,extend,display) {
    if(extend =="json"){
        var obj = JSON.parse(fileText);
        var results = obj.Result;

        for(var i = 0; i < results.length; i++) { 
            var elementDom = buildDOMFromElement( results[i].title, results[i].description, results[i].url );
            display.appendChild(elementDom);
        }
    } else if (extend =="csv"){
        var text =fileText.split('\n');

        for (var i = 0; i < text.length-1; i++) {
            var elements = text[i].split(',');
            var elementDom = buildDOMFromElement( elements[0], elements[2], elements[1] );
            display.appendChild(elementDom);
        }
    } else if (extend =="xml") {
        var parser = new DOMParser();
        var xmlobj = parser.parseFromString(fileText,"text/xml");
        var results =xmlobj.getElementsByTagName("result");

        for(var i = 0; i < results.length; i++) {
            var elementDom = buildDOMFromElement( 
                    xmlobj.getElementsByTagName("title")[i].textContent,
                    xmlobj.getElementsByTagName("description")[i].textContent,
                    xmlobj.getElementsByTagName("url")[i].textContent
                );
            display.appendChild(elementDom);
        }
    }
}