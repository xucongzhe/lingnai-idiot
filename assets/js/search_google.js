// Search from Google

var searchForm = document.getElementById('search-form');

searchForm.addEventListener('submit', function(){
    var search = this.querySelector('#search').value;
    var apiUrl = 'https://www.googleapis.com/customsearch/v1';
    var apiKey = 'AIzaSyDg9ejezvRysdC8RuU7-WQvHH1ivGA09FU';
    var cx = '010984141132717841019:orxel63ipxf';
    var requestUrl = apiUrl + '?' + 'key=' + apiKey + '&cx=' + cx + '&q=' + search;
    var resultElement = document.getElementById('results');

    // Clear result box before each search
    resultElement.innerHTML = '';

    // Initiate HTTP request to Google Search API
    var xhttp = new XMLHttpRequest();
    xhttp.open("GET", requestUrl, false);
    xhttp.send();
    var data = JSON.parse(xhttp.responseText);
    if(data.items){
        data.items.forEach(function(item) {
            console.log(item);
            var elementsDom = buildDOMFromElement( item.title, item.snippet, item.formattedUrl );
            resultElement.append(elementsDom);
        });
    }else{
        var errorMessage = document.createElement("p");
            errorMessage.innerHTML = 'No result found, please try again.';
            resultElement.append(errorMessage);
    }
});